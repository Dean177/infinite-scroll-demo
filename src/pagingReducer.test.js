import { pagingReducer, initialState } from './pagingReducer'

let makeFilms = (from, to) =>
  new Array(to + 1 - from).map((_, index) => ({ id: index + from }))

describe('pagingReducer', () => {
  it(`throws when receiving an action in doesn't know how to handle`, () => {
    expect(() => pagingReducer(initialState, { kind: 'unknown' })).toThrow()
  })

  it(`sets 'after' to the last film's id when the 'fetch-more' action is received`, () => {
    let state = {
      after: null,
      hasMore: true,
      films: [{ id: '1' }, { id: '2' }, { id: '3' }],
    }

    expect(pagingReducer(state, { kind: 'fetch-more' })).toEqual({
      ...state,
      after: '3',
    })
  })

  it('appends new films to the end of its films array', () => {
    let state = {
      after: '3',
      hasMore: true,
      films: [{ id: '1' }, { id: '2' }, { id: '3' }],
    }

    let newState = pagingReducer(state, {
      kind: 'new-page',
      payload: [{ id: '4' }, { id: '5' }, { id: '6' }],
    })

    expect(newState).toEqual({
      after: '3',
      hasMore: true,
      films: [
        { id: '1' },
        { id: '2' },
        { id: '3' },
        { id: '4' },
        { id: '5' },
        { id: '6' },
      ],
    })
  })

  it('sets hasMore to false when the new films are smaller than "pageSize"', () => {
    let state = {
      after: '3',
      hasMore: true,
      films: makeFilms(1, 3),
    }

    let newState = pagingReducer(state, {
      kind: 'new-page',
      payload: makeFilms(4, 5),
    })

    expect(newState).toEqual({
      after: '3',
      hasMore: false,
      films: makeFilms(1, 5),
    })
  })

  it(`reverts to the initial state when the 'reset' event is received`, () => {
    let state = {
      after: '3',
      hasMore: true,
      films: [{ id: '1' }, { id: '2' }, { id: '3' }],
    }

    let newState = pagingReducer(state, {
      kind: 'reset',
    })

    expect(newState).toBe(initialState)
  })
})

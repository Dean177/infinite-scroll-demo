import React from 'react'
import { fireEvent, render } from '@testing-library/react'
import { InfiniteList } from './InfiniteList'

let range = (length) => new Array(length).map((_, index) => index)

describe('InfiniteList', () => {
  it('calls on endReached once its contents has been scrolled to within endThreshold of the end of its content', () => {
    let initialItems = range(3)
    const callback = jest.fn()
    const { getByRole } = render(
      <InfiniteList
        items={initialItems}
        onEndReached={callback}
        hasMore={true}
        renderItem={(n) => <div>{n}</div>}
      />,
    )
    let scrollContainer = getByRole('list')

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 200,
      },
    })

    expect(callback).toHaveBeenCalled()
  })

  it('calls on endReached again if its contents has been scrolled further', () => {
    const callback = jest.fn()
    const { getByRole, rerender } = render(
      <InfiniteList
        items={range(3)}
        onEndReached={callback}
        hasMore={true}
        renderItem={(n) => <div>{n}</div>}
      />,
    )
    let scrollContainer = getByRole('list')

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 200,
      },
    })

    rerender(
      <InfiniteList
        items={range(6)}
        onEndReached={callback}
        hasMore={true}
        renderItem={(n) => <div>{n}</div>}
      />,
    )

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 300,
      },
    })

    expect(callback).toHaveBeenCalledTimes(2)
  })

  it('will not call onEndReached again if hasMore is false', () => {
    const callback = jest.fn()
    const { getByRole, rerender } = render(
      <InfiniteList
        items={range(3)}
        onEndReached={callback}
        hasMore={true}
        renderItem={(n) => <div>{n}</div>}
      />,
    )
    let scrollContainer = getByRole('list')

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 200,
      },
    })

    rerender(
      <InfiniteList
        items={range(5)}
        onEndReached={callback}
        hasMore={false}
        renderItem={(n) => <div>{n}</div>}
      />,
    )

    fireEvent.scroll(scrollContainer, {
      target: {
        scrollTop: 300,
      },
    })

    expect(callback).toHaveBeenCalledTimes(1)
  })
})

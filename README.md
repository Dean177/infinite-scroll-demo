# Infinite scroll challenge

[![Netlify Status](https://api.netlify.com/api/v1/badges/a00b4581-1a08-4a89-b62c-3d166ba90856/deploy-status)](https://app.netlify.com/sites/kairos-infinite-scroll-challenge/deploys)

This app lets you find and play the opening sequence from one of the Star Wars movies.

The 'infinite' scroll effect is pretty subtle for a couple of reasons:
- We start fetching the next page _before_ the user reaches the end of the list
- The API we send our requests too is pretty speedy
- There isn't that much content

You can witness the behavior more easily by pretending you are on a slow connection, this can be done in Chrome by: 
- Opening the developer tools
- Heading to the network tab
- Selecting the dropdown labelled 'Online' then selecting 'Slow 3G'

Hopefully this is still sufficient to demonstrate the _technique_ involved in 'infinite scrolling'.

You can play with it at [https://infinite-scroll-demo.deanmerchant.com]()

## Highlights

- Responsive, layout supports mobile devices to large high resolution displays
- Subtle transitions between states & screens
- You can 'play' the opening for each movie

### Dependencies

I used a number of libraries

- [react-spring](https://www.react-spring.io) Makes writing declarative animations absolutely painless. I used this for the fade-in and fade-out effects. 
- [styled-components](https://styled-components.com) I find the `css` prop to be a really convenient way to style components. 
- [urql](https://formidable.com/open-source/urql/) a super slim graphql client. A little too slim it turns out ‍as it doesn't include a way to paginate out of the box. 
- [testing-library](https://testing-library.com) This makes testing the scroll events for `InfiniteList` simple
- [jest](https://jestjs.io) The best javascript test runner around
    
## Approach

I bootstrapped my project with [create-react-app](https://facebook.github.io/create-react-app/). It bundles lots of useful functionality and is a real time saver.  

I used the [Star Wars graphql API](https://swapi.graph.cool) mainly because I had an idea to make the demo more 'app' like (make sure you hit 'play').

It turned out to have a couple of shortcomings:
- The API provides no way to query for the total number of films. As a consequence I needed to implement some extra logic to figure out when the app needed to stop making requests.
- There isn't much data, there just aren't films, but hopefully the small page sizes (we only fetch three films at once) mean this demonstrates the 'Infinite loading' approach.
- The API is out of date. The newest couple of films don't show up!

The main components responsible are `InfiniteList` and `pagingReducer`.

`InfiniteList` is responsible for displaying the list of results and calling it's `onEndReached` when the user scrolling nears the end of the list.

`pagingReducer` handles the events which change the list displayed to the user. Things like appending new results when they come in or clearing the results when the user applies a filter.

These assertions are verified by their respective test files. 

### Additional considerations

Since the app is so small there are a couple of things that are slightly different from how I would do things on a commercial project.

#### Everything is in one file

Almost all the code lives in `App.js`. 

For such a small demo app that realistically is never going to be touched again its much more efficient to have everything in one place.

Once an app starts to span multiple screens, it's time to start thinking more clearly about component dependencies and which bits should be moved into their own files.

#### Types

I am a big fan of [Typescript](https://www.typescriptlang.org), particularly for larger codebases. It can be immensely helpful for teams, especially when new people need to be introduced to a codebase.

For this project it would only be a hindrance for a couple of reasons:
- I have no idea if the reader is familiar with it
- There is nothing particularly complicated going on
- The code isn't going to be worked on by anyone else
- I am probably never going to work on it again (Its "done")


#### Testing

Since this is only going to be used by you and I, I didn't spend any time on browser compatibility. It can be a *huge* timesink.

I did test on the following platforms:
- Mobile Safari
- Chrome on OSX

For internal business applications there is usually the ability to restrict the number of supported platforms.
    
For consumer facing projects I have typically used a combination of integration tests, dedicated testers and [https://www.browserstack.com/]() to ensure applications were usable by the desired audience (often discovered with the help of analytics).

This seemed well out of scope for this project.

## Development 

You first need [Node](https://nodejs.org/en/) installed locally, then in the project directory, you can run:

### `npm install` 

To fetch the dependencies

### `npm start`

To runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in its interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.

You won't need to do this as building and deployment is handled automatically (see the next section).

## Deployment

This is done automatically when committing to the `master` branch via [Netlify](https://netlify.com).
